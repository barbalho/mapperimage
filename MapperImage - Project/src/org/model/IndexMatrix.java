package org.model;

/**
 * This class represents an item in the array, with coordinated and the item in the array.
 * @author Felipe Barbalho Rocha, Raul Silveira Silva
 * @version 04.11.2015
 */
public class IndexMatrix {
    public int x;
    public int y;
    private int value;

    /**
     * Constructor
     * @param x X index of the matrix
     * @param y Y index of the matrix
     * @param value value of the matrix (value = matrix[x][y])
     */
    public IndexMatrix(int x, int y, int value) {
        this.x = x;
        this.y = y;
        this.value = value;
    }
    
    /**
     * Get the element X 
     * @return the element X 
     */
    public int getX() {
        return x;
    }

    /**
     * Set the element X 
     * @param x the element X 
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * Get the element Y 
     * @return the element Y 
     */
    public int getY() {
        return y;
    }

    /**
     * Set the element Y 
     * @param y the element Y 
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * Get the value = matrix[x][y]
     * @return the value
     */
    public int getValue() {
        return value;
    }

    /**
     * Set the value
     * @param value the value
     */
    public void setValue(int value) {
        this.value = value;
    }
}