package org.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is responsible for storing the Image MAP is a map of labels so that it can be persisted.
 * @author Felipe Barbalho Rocha, Raul Silveira Silva
 * @version 04.11.2015
 */
public class DictionaryImageMap implements Serializable{
    private ImageMap imageMap;
    private Map<String,ArrayList<Integer>> mapLabels ;

    /**
     * Construct
     * @param imMap Map labels object
     */
    public DictionaryImageMap(ImageMap imMap) {
        this.imageMap = imMap;
        mapLabels = new HashMap<>();
    }

    /**
     * get ImageMap
     * @return ImageMap
     */
    public ImageMap getImageMap() {
        return imageMap;
    }

    /**
     * set new ImageMap
     * @param imageMap new ImageMap 
     */
    public void setImageMap(ImageMap imageMap) {
        this.imageMap = imageMap;
    }
    
    /**
     * get MapLabels
     * @return MapLabels
     */
    public Map<String, ArrayList<Integer>> getMapLabels() {
        return mapLabels;
    }

    /**
     * set MapLabels
     * @param mapLabels mapLabels
     */
    public void setMapLabels(Map<String, ArrayList<Integer>> mapLabels) {
        this.mapLabels = mapLabels;
    }
    
    /**
     * Set value in maplabels.
     * @param key key of map.
     * @param values new value.
     */
    public void setValueInMaplabels(String key, ArrayList<Integer> values){
         this.mapLabels.put(key, values);
    }
}