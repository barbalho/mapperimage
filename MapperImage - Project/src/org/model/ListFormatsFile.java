package org.model;

import java.util.HashMap;
import java.util.Map;

/**
 * This class describes a list of formats with their respective extensions
 * @author Felipe Barbalho Rocha, Raul Silveira Silva
 * @version 04.11.2015
 */
public class ListFormatsFile {
    private Map<String,String> formats;

    /**
     * Construct of ListFormatsFile.
     */
    public ListFormatsFile() {
        this.formats = new  HashMap<>();
    }
    
    /**
     * Add new format to map.
     * @param format file format.
     * @param extension file extension.
     */
    public void add(String format, String extension) {
       formats.put(format, extension);
    }
    
    /**
     * Get the map formats.
     * @return map formats. 
     */
    public Map<String, String> getFormats() {
        return formats;
    }

    /**
     * Set the map formats.
     * @param formats map format.
     */
    public void setFormats(Map<String, String> formats) {
        this.formats = formats;
    }

}