package org.model;

import boofcv.struct.image.ImageSInt32;
import br.ufrn.imd.lp2.imagesegmentation.ImageInformation;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.Arrays;
import org.util.ImageCustomizerUtil;

/**
 * This class extends Image and Information is responsible for the bank logic information image.
 * @author Felipe Barbalho Rocha, Raul Silveira Silva
 * @version 04.11.2015
 */
public class ImageMap extends ImageInformation implements Serializable{
    int[][] matrixRegionMarkedPixels;
    double blurLevel;
    int colorRadius;
    int minSize;
    /**
     * Constructor
     * @param numberOfRegions parameter segmentation number Of Regions
     * @param originalImage parameter segmentation original Image
     * @param regionMarkedImage parameter segmentation region Marked Image
     * @param segmentedImageMap type of segmentation
     */
    public ImageMap(int numberOfRegions, BufferedImage originalImage, BufferedImage regionMarkedImage, ImageSInt32 segmentedImageMap) {
        super(numberOfRegions, originalImage, regionMarkedImage, segmentedImageMap);
        initMatrixRegionMarkedPixels();
    }
    
    /**
     * Constructor
     * @param inf Image Information already segmented
     * @param blurLevel blurLevel param of segmentation
     * @param colorRadius colorRadius param of segmentation
     * @param minSize  minSize param of segmentation
     */
    public ImageMap(ImageInformation inf, double blurLevel, int colorRadius, int minSize) {
        super(inf.getTotalRegions(), inf.getOriginalImage(), inf.getRegionMarkedImage(), ImageCustomizerUtil.intToImageSInt32(inf.getSegmentedImageMap()));
        this.blurLevel = blurLevel;
        this.colorRadius = colorRadius;
        this.minSize = minSize;
        initMatrixRegionMarkedPixels();
    }

    /**
     * Overwrite the labels vector transposed into a common vector.
     * @return labels vector.
     */
    @Override
    public int[] getRegionMarkedPixels() {
        int newVector[] = new int [super.getRegionMarkedPixels().length];
        int k = 0;
        for (int i = 0; i < getHeight(); i++) {
            for (int j = 0; j < getWidth(); j++) {
                 newVector[i+getWidth()] = super.getSegmentedImageMap()[k++];
            }        
        }
        return newVector;
    }
    
    /**
     * Initializes labels matrix.
     */
    private void initMatrixRegionMarkedPixels(){
        matrixRegionMarkedPixels = new int[getWidth()][getHeight()];
        int k = 0;
        for (int i = 0; i < getHeight(); i++) {
            for (int j = 0; j < getWidth(); j++) {
                int map = super.getSegmentedImageMap()[k++];
                matrixRegionMarkedPixels[j][i] = map;
            }        
        }
    }

    /**
     * Gets labels matrix.
     * @return labels matrix.
     */
    public int[][] getMatrixRegionMarkedPixels() {
        return matrixRegionMarkedPixels;
    }

    /**
     * Sets labels matrix.
     * @param matrixRegionMarkedPixels labels matrix.
     */
    public void setMatrixRegionMarkedPixels(int[][] matrixRegionMarkedPixels) {
        this.matrixRegionMarkedPixels = matrixRegionMarkedPixels;
    }

    /**
     * return String with description of the image map object.
     * @return toString()
     */
    @Override
    public String toString() {
        return "ImageMap{" + "matrixRegionMarkedPixels=" + Arrays.toString(matrixRegionMarkedPixels) + '}';
    }
}