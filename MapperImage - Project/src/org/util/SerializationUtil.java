package org.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
 
/**
 * This class is responsible for object serialization and deserialization files
 * @author Felipe Barbalho Rocha, Raul Silveira Silva
 * @version 04.11.2015
 * @author pankaj
 * @version 27.10.2013
 * @fonte http://www.journaldev.com/2452/java-serialization-example-tutorial-serializable-serialversionuid

 */
public class SerializationUtil {
 
    /**
     * Deserialize to Object from given file
     * @param fileName file name
     * @return read object
     */
    public static Object deserialize(String fileName) {
        try{
            FileInputStream fis = new FileInputStream(fileName);
            Object obj;
            ObjectInputStream ois = new ObjectInputStream(fis);
            obj = ois.readObject();
            return obj;
        }catch(Exception ex){
            System.err.println("Erro ao Deserializar!");
            return null;
        }
    }
 
    /**
     * Serialize the given object and save it to file
     * @param obj object to be saved.
     * @param fileName file name
     * @throws IOException In the event of errors in saving.
     */
    public static void serialize(Object obj, String fileName) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(fileName)) {
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(obj);
        }
    }
 
}