package org.util;

//Só pode apartir da versão 8u65 do JDK
//import java.util.Optional;
//import javafx.scene.control.Alert;
//import javafx.scene.control.ButtonType;
import javax.swing.JOptionPane;

/**
 * This class has methods for displaying dialog boxes with JavaFX.
 * @author Felipe Barbalho Rocha, Raul Silveira Silva
 * @version 04.11.2015
 */
public class AlertMessage {
    
    /**
     * Dialog box for confirmation message.
     * @param title Message title.
     * @param header Message header.
     * @param message Detailed message.
     * @return User response event (if OK or not).
     */
    public static boolean confirmDialog(String title, String header, String message) {
        
            //Só pode apartir da versão 8u65 do JDK
            /*Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            if (title != null) {
                alert.setTitle(title);
            } else {
                alert.setTitle("Confirmation");
            }
            if (header != null) {
                alert.setHeaderText(header);
            }
            if (message != null) {
                alert.setContentText(message);
            }
            Optional<ButtonType> result = alert.showAndWait();
            return result.get() == ButtonType.OK;*/
        
            if(message==null) message = "";
            return JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog( null,header+", "+message,"Confirmation" ,JOptionPane.YES_NO_OPTION);  
    }
    
    /**
     * Show Warnning message to user
     * @param title Warnning Message title.
     * @param header Warnning Message header.
     * @param message Warnning Detailed message.
     //* @return User response event (if OK or not).
     */
    public static void showWarnning(String title, String header, String message) {
        //Só pode apartir da versão 8u65 do JDK
        /*Alert alert = new Alert(Alert.AlertType.WARNING);
        if (title != null) {
        } else {
            alert.setTitle("Warning");
        }
        if (header != null) {
            alert.setHeaderText(header);
        }
        if (message != null) {
            alert.setContentText(message);
        }
        Optional<ButtonType> result = alert.showAndWait();
        if (!result.isPresent())
            return false;
        return result.get() == ButtonType.OK;*/
        if(message==null) message = "";
        JOptionPane.showMessageDialog(null,header+", "+message, "Warning",JOptionPane.WARNING_MESSAGE);  
    }
    
    /**
     * Show message to user
     * @param title Message title.
     * @param header Message header.
     * @param message Detailed message.
     //* @return User response event (if OK or not).
     */
    public static void showMessage(String title, String header, String message) {
        //Só pode apartir da versão 8u65 do JDK  -> e o retorno era boolean
        /*Alert alert = new Alert(Alert.AlertType.INFORMATION);
        if (title != null) {
            alert.setTitle(title);
        } else {
            alert.setTitle("Information");
        }
        if (header != null) {
            alert.setHeaderText(header);
        }
        if (message != null) {
            alert.setContentText(message);
        }
        Optional<ButtonType> result = alert.showAndWait();
        if (!result.isPresent())
            return false;
        return result.get() == ButtonType.OK;*/
        
        JOptionPane.showMessageDialog(null,message, header,JOptionPane.INFORMATION_MESSAGE);  
    }
    
    static public void LOG(String logTitle, String message){
        System.out.println(logTitle+" "+message);
    }
    
}