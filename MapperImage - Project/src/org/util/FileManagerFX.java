package org.util;

import org.model.ListFormatsFile;
import java.io.File;
import javafx.stage.FileChooser;
import javafx.stage.Window;

/**
 * This Class It is responsible for manipulating files.
 * @author Felipe Barbalho Rocha, Raul Silveira Silva
 * @version 04.11.2015
 */
public class FileManagerFX {
    public static final ListFormatsFile FORMAT_IMAGE = getFileImageExtension();
    public static final ListFormatsFile FORMAT_DMAP = getFileDMAPExtension();
    
    /**
     * get File Image Extension.
     * @return Extensions Image File.
     */
    private static ListFormatsFile getFileImageExtension(){
        ListFormatsFile formats = new ListFormatsFile();
        formats.add("All Images", "*.*");
        formats.add("JPG", "*.jpg");
        formats.add("GIF", "*.gif");
        formats.add("BMP", "*.bmp");
        formats.add("PNG", "*.png");
        return formats; 
    }
    
    /**
     * Get DMAP File Extension.
     * @return DMAP File Extension.
     */
    private static ListFormatsFile getFileDMAPExtension(){
        ListFormatsFile formats = new ListFormatsFile();
        formats.add("DMAP", "*.dmap");
        formats.add("All Files", "*");
        return formats; 
    }
    
    /**
     * Open a file through dialog.
     * @param title File title.
     * @param diretory File directory.
     * @param formats File formats list.
     * @param window reference to page resizing.
     * @return file selected or null, otherwise.
     */
    public static File openFile(String title, String diretory, ListFormatsFile formats, Window window){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(title);
        for(String key : formats.getFormats().keySet()){
            fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter(key, formats.getFormats().get(key))
            );
        }
        File file = null;
        try{
            fileChooser.setInitialDirectory(new File(diretory)); 
            file = fileChooser.showOpenDialog(window);
        }catch(IllegalArgumentException erro){
            System.err.println("Pasta não existe!");
            fileChooser.setInitialDirectory(null); 
            file = fileChooser.showOpenDialog(window);
        }
        return file;
    }
    
    /**
     * Save dmap file.
     * @param title title file.
     * @param diretory diretory to save.
     * @param formats extension file.
     * @param window reference to page resizing.
     * @return File path.
     */
    public static File saveFile(String title, String diretory, ListFormatsFile formats, Window window) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save DMAP");
        
        for(String key : formats.getFormats().keySet()){
            fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter(key, formats.getFormats().get(key))
            );
        }
        File file = null;
        try{
            fileChooser.setInitialDirectory(new File(diretory)); 
            file = fileChooser.showSaveDialog(window);
        }catch(IllegalArgumentException erro){
            System.err.println("Pasta não existe!");
            fileChooser.setInitialDirectory(null); 
            file = fileChooser.showSaveDialog(window);
        }
        return file;
    }
    
    
}