package org.util;

import boofcv.struct.image.ImageSInt32;
import org.model.IndexMatrix;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;

/**
 * This class has several methods for imaging in the system.
 * @author Felipe Barbalho Rocha, Raul Silveira Silva.
 * @version 04.11.2015
 */
public class ImageCustomizerUtil {

    /**
     * Paint each label map with a gray gradient.
     * @param matrix matrix with labels.
     * @param levelGradient integer level gradient.
     * @return new image with gray gradient.
     */
    public static BufferedImage paintMapLabelGrayGradient(int matrix[][], int levelGradient){
        int x = matrix.length;
        int y = matrix[0].length;
        BufferedImage grayImage = new BufferedImage(x, y, BufferedImage.TYPE_INT_RGB);
        Color cor;
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                if(levelGradient < 255){
                    int tomCinza = matrix[i][j] *(255/levelGradient);
                    cor = new Color(tomCinza,tomCinza,tomCinza);
                }else{
                    cor = new Color(levelGradient%(matrix[i][j]+1)*10000);
                }
                grayImage.setRGB(i,j,cor.getRGB()); 
            }        
        }
        return grayImage;
    }
    
    /**
     * Increase brightness of the pixel.
     * @param pixel pixel of image.
     * @param brightnessLevel brightness Level applied to the pixel.
     * @return new pixel Color.
     */
    public static Color increaseBrightnessPixel(Color pixel, double brightnessLevel){
        int r = (int)(pixel.getRed()*brightnessLevel);
        int g = (int)(pixel.getGreen()*brightnessLevel);
        int b = (int)(pixel.getBlue()*brightnessLevel);
        if(r>255) r=255;
        if(g>255) g=255;
        if(b>255) b=255;
        return new Color(r,g,b);
    }
    
    /**
     * Decrease brightness of the pixel.
     * @param pixel pixel of image.
     * @param brightnessLevel brightness Level applied to the pixel.
     * @return new pixel Color.
     */
    public static Color decreaseBrightnessPixel(Color pixel, double brightnessLevel){
        int r = (int)(pixel.getRed()/brightnessLevel);
        int g = (int)(pixel.getGreen()/brightnessLevel);
        int b = (int)(pixel.getBlue()/brightnessLevel);
        if(r<1) r=1;
        if(g<1) g=1;
        if(b<1) b=1;
        return new Color(r,g,b);
    }
    
    /**
     * Apply shine in a region of the image and darkens the rest.
     * @param image original image to change.
     * @param matrix labels matrix.
     * @param regions Labels to apply brightness.
     * @param brightnessLevel brightness Level applied to the pixels of regions.
     * @return new image with brightness of focus in the specified region.
     */
    public static BufferedImage imageBrightnessFocusInRegions(BufferedImage image, int matrix[][], ArrayList<Integer> regions, double brightnessLevel ){
        for (int i = 0; i < image.getWidth(); i++) {
            for (int j = 0; j < image.getHeight(); j++) {
                Color oldColor = new Color(image.getRGB(i, j));
                Color newColor;
                if(regions.contains(matrix[i][j])){
                    newColor = increaseBrightnessPixel(oldColor, brightnessLevel);
                }else{
                    newColor = decreaseBrightnessPixel(oldColor, brightnessLevel);
                }
                image.setRGB(i, j, newColor.getRGB());
            }        
        }
        return image;
    }
    
    /**
     * Convert list IndexMatrixs to list labels.
     * @param points coords de matix.
     * @param matrix labels matrix.
     * @return 
     */
    public static ArrayList<Integer> listPointsToListValues(ArrayList<IndexMatrix> points, int matrix[][]){
        ArrayList<Integer> regionsInt = new ArrayList<>();
        points.stream().forEach((r) -> {
            regionsInt.add(r.getValue());
        });
        return regionsInt;
    }
    
    /**
     * Convert ArrayList IndexMatrix to list IndexMatrixs.
     * @param labels ArrayList de labels integer.
     * @param matrix labels matrix.
     * @return 
     */
    public static ArrayList<IndexMatrix> listValuesToListPoints(ArrayList<Integer> labels, int matrix[][]){
        ArrayList<IndexMatrix> points = new ArrayList<>();
        boolean exit;
        for (int l : labels) {
            for(int i = 0; i < matrix.length; i++){
                exit = false;
                for (int j = 0; j < matrix[i].length; j++) {
                    if(l == matrix[i][j]){
                        points.add(new IndexMatrix(i,j,matrix[i][j]));
                        exit = true;
                    }
                }
                if(exit) break;
            }
        }
        return points;
    }
      
    /**
     * Apply shine in a region of the image and darkens the rest.
     * @param image original image to change.
     * @param matrix coordenadas matrix.
     * @param regions Labels to apply brightness.
     * @param brightnessLevel brightness Level applied to the pixels of regions.
     * @return new image with brightness of focus in the specified region.
     */
    public static BufferedImage imageBrightnessFocusInRegionsWithPoint(BufferedImage image, int matrix[][], ArrayList<IndexMatrix> regions, double brightnessLevel ){
        ArrayList<Integer> regionsInt = listPointsToListValues(regions,matrix);
        return imageBrightnessFocusInRegions(image, matrix, regionsInt, brightnessLevel );
    }
    
    /**
     * Convert int[] in ImageSInt32 type
     * @param mapInt arry de inteiros
     * @return ImageSInt32 type
     */
    public static ImageSInt32 intToImageSInt32(int mapInt []){
        ImageSInt32 mapInt32 = new ImageSInt32();
        mapInt32.setData(mapInt); 
        return mapInt32;
    }
    
    /**
     * Convert BufferedImage in Image type
     * @param bf image BufferedImage type 
     * @return Image type
     */
    public static Image toImage(BufferedImage bf){
        WritableImage wi = new WritableImage(bf.getWidth(), bf.getHeight());
        Image image = SwingFXUtils.toFXImage(bf, wi);
        return image;
    }
}