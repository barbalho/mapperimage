package org.dao;

import java.util.ArrayList;

/**
 * Class for a trie tree node
 * @author Felipe Barbalho Rocha and Raul Silveira Silva
 */
public class TrieNode {
    private int wordsCount = 0;
    private ArrayList<TrieNode> childrenNodes;
    private String prefix;
    private boolean endWord;
    
    /**
     * TrieNode's constructor
     */
    public TrieNode(){
        this.childrenNodes = new ArrayList<>();
        this.prefix = "";
    }

    /**
     * TrieNode's constructor
     * @param prefix Word on node
     */
    public TrieNode(String prefix) {
        this.prefix = prefix;
    }
    
    /**
     * TrieNode's constructor
     * @param prefix Word on node
     */
    public TrieNode(char prefix) {
        this.childrenNodes = new ArrayList<>();
        this.prefix = ""+prefix;
    }

    /**
     * Gets the sons of the node
     * @return ArrayList with the sons
     */
    public ArrayList<TrieNode> getChildrenNodes() {
        return childrenNodes;
    }
    
    /**
     * Gets a son of the node with an especific prefix
     * @param prefix Prefix of the son
     * @return Node son of the TrieNode
     */
    public TrieNode getChildrenNode(String prefix) {
        for (TrieNode childrenNode : childrenNodes) {
            if(childrenNode.getPrefix().equals(prefix)){
                return childrenNode;
            }
        }
        return null;
    }
    
    /**
     * Gets a son of the node with an especific prefix
     * @param prefix Prefix of the son
     * @param ignoreCase For case sensitive
     * @return Node son of the TrieNode
     */
    public TrieNode getChildrenNode(char prefix) {
        String aux = prefix+"";
        for (TrieNode childrenNode : childrenNodes) {
            
            if(aux.equalsIgnoreCase(childrenNode.getPrefix())){
                return childrenNode;
            }
        }
        return null;
    }
    
    /**
     * Add a son on the TrieNode
     * @param childrenNode TrieNode to be inserted
     */
    public void setChildrenNode(TrieNode childrenNode) {
        this.childrenNodes.add(childrenNode);
    }

    /**
     * Add a set of sons on the TrieNode
     * @param childrenNodes ArrayList of TrieNodes
     */
    public void setChildrenNodes(ArrayList<TrieNode> childrenNodes) {
        this.childrenNodes = childrenNodes;
    }

    /**
     * Gets the prefix of the node
     * @return Prefix of the node
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * Add a prefix for the node
     * @param prefix Prefix for the node
     */
    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
    
    /**
     * Increase the word counter
     */
    void increaseWordsCount(){
        wordsCount++;
    }
    
    /**
     * Decrease the word counter
     */
    void decreaseWordsCount(){
        wordsCount--;
    }

    /**
     * Gets the counter value
     * @return Counter value
     */
    public int getWordsCount() {
        return wordsCount;
    }

    /**
     * Define the counter value
     * @param wordsCount Counter value
     */
    public void setWordsCount(int wordsCount) {
        this.wordsCount = wordsCount;
    }

    /**
     * Check if is a leaf node
     * @return Boolean value to inform if is a leaf node or not
     */
    public boolean isEndWord() {
        return endWord;
    }

    /**
     * Defines if the node is a leaf or not
     * @param endWord True if is a leaf or False else
     */
    public void setEndWord(boolean endWord) {
        this.endWord = endWord;
    }

    /**
     * toString Method
     */
    @Override
    public String toString() {
        return "TrieNode{" + "wordsCount=" + wordsCount + ", childrenNodes=" + childrenNodes + ", prefix=" + prefix + ", endWord=" + endWord + '}';
    }
     
}
