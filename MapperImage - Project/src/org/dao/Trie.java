package org.dao;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for a trie tree
 * @author Felipe Barbalho Rocha and Raul Silveira Silva
 */
public class Trie {
    private final TrieNode root;
    
    /**
     * Trie's constructor
     */
    public Trie(){
        this.root = new TrieNode();
    }
    
    /**
     * Add a word on the tree
     * @param word Word to be inserted
     */
    public void insertWord(String word){
        word = word.toLowerCase();
        TrieNode newNode = root;
        newNode.increaseWordsCount();
        while(!word.isEmpty()){
            TrieNode auxNode = newNode.getChildrenNode(word.charAt(0));
            
            if(auxNode!=null){
                
                newNode = auxNode;
            }else{
                TrieNode nNode = new TrieNode(word.charAt(0));
                newNode.setChildrenNode(nNode);
                newNode =  newNode.getChildrenNodes().get(newNode.getChildrenNodes().indexOf(nNode));
            }
            newNode.increaseWordsCount();
            word = word.substring(1, word.length());
        }
        newNode.setEndWord(true);
    }
    
    
    /**
     * Remove a word form the tree
     * @param word Word to be removed
     * @return Boolean value to inform if the word was removed or not
     */
    public boolean removeWord(String word){
        word = word.toLowerCase();
        TrieNode tNode = root;
        if(searchWord(word)){
            tNode.decreaseWordsCount();
            while(! word.isEmpty()){
                TrieNode childrenNode = tNode.getChildrenNode(word.charAt(0));
                if(childrenNode!=null && childrenNode.getWordsCount()==1){
                    tNode.getChildrenNodes().remove(childrenNode);
                    childrenNode = null;
                    break;
                }else{
                    childrenNode.decreaseWordsCount();
                }
                word = word.substring(1, word.length());
                if(! word.isEmpty()){
                    tNode = tNode.getChildrenNode(word.charAt(0));
                    tNode.decreaseWordsCount();
                }
            }
            return true;
        }
        return false;
    }
    

    /**
     * Find a word on the tree
     * @param word Word to be find
     * @return Boolean value to inform if the word is on the tree or not
     */
    public boolean searchWord(String word){
        word = word.toLowerCase();
        TrieNode tNode = root;
        while(! word.isEmpty()){
            if(tNode != null){
                tNode = tNode.getChildrenNode(word.charAt(0));
            }
            if(tNode==null){
                return false;
            }
            word = word.substring(1, word.length());
        }
        return true;
    }
    
    
    /**
     * Gets a set of words with an especific prefix
     * @param preffix Prefix of the word
     * @return List of words
     */
    public List getWordsFromPrefix(String preffix){
        preffix = preffix.toLowerCase();
        ArrayList<String> list = new ArrayList<>();
        TrieNode node = root;
        String tempPreffix = preffix;
        while(!"".equals(tempPreffix)){
            node = node.getChildrenNode(tempPreffix.charAt(0));
            if(null==node){
                return null;
            }
            tempPreffix = tempPreffix.substring(1, tempPreffix.length());
        }
        if(preffix.isEmpty()){
            return getWordsFromPrefix("", node, list);
        }
        return getWordsFromPrefix(preffix.substring(0, preffix.length()-1), node, list);
    }
    
    /**
     * Gets a number of words with an especific prefix
     * @param preffix Prefix of the word
     * @return Number of words
     */
    public int getWordCountFromPreffix(String preffix){
        preffix = preffix.toLowerCase();
        TrieNode node = root;
        int wordCount = 0;
        while(!"".equals(preffix)){
            node = node.getChildrenNode(preffix.charAt(0));
            if(node!=null){
                wordCount = node.getWordsCount();
            }else{
                break;
            }
            preffix = preffix.substring(1, preffix.length());
        }
        return wordCount;
    }
    
    /**
     * Prints the trie tree on the console
     * @param node Tree's node
     * @param value Word on node
     */
    public  void print(TrieNode node, String value){
        if(!node.getChildrenNodes().isEmpty()){
            for (TrieNode n : node.getChildrenNodes()) {
                if(n.isEndWord()){
                    System.out.println("-"+value + " " + n.getPrefix()+"*");
                }else{
                    System.out.println("-"+value + " " + n.getPrefix());
                }
                print(n, "-"+ value + " " + n.getPrefix());
            }
        }

    }
    
    /**
     * Gets a set of words with an especific prefix
     * @param preffix Prefix of the word
     * @param node Tree's node
     * @param list ArrayList for store the words
     * @return ArrayList of words
     */
    private ArrayList<String> getWordsFromPrefix(String preffix, TrieNode node, ArrayList<String> list){
        preffix = preffix.toLowerCase();
        if(node.isEndWord()){
            list.add(preffix+node.getPrefix());
        }
        for (TrieNode tn : node.getChildrenNodes()) {
            list = getWordsFromPrefix(preffix+node.getPrefix(), tn, list);
        }
        return list;
    }
    
    /**
     * Prints the trie tree on the console
     */
    public void printTrie(){
        print(root, root.getPrefix());
    }

    /**
     * Gets the root of the tree
     * @return TrieNode of the trie tree
     */
    public TrieNode getRoot() {
        return root;
    }
}
