package org.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.util.AlertMessage;

/**
 * Class for provide all operations with the database
 * @author Felipe Barbalho Rocha and Raul Silveira Silva
 */
public class Database {
    private final String LOG_TITLE = "[DATA-BASE]";
    
    private final String url;
    private Connection con;
    private Statement stmt;
    private ResultSet rs;

    /**
     * Database's constructor.
     * @param className ClassName JDBC.
     * @param url ulr conection database.
     */
    public Database(String className, String url) {
        try {
            Class.forName(className);
        } catch (ClassNotFoundException ex) {
             AlertMessage.LOG(LOG_TITLE, "Error starting JDBC!");
        }
        this.url = url;
    }
    
    /**
     * Create a labels table.
     * @param sqlCreation string containing the SQL for table creation.
     */
    public void createTable(String sqlCreation) {
        try{
            executer(sqlCreation, 1, true);
            AlertMessage.LOG(LOG_TITLE, "Table successfully Built!");
        }catch(SQLException ex){
            AlertMessage.LOG(LOG_TITLE, "Table already exists!");
        }
    }
    
    /**
     * Add data on database.
     * @param sql sql for inserction.
     * @param closeConnection Boolean value for close the Database connection.
     * @throws java.sql.SQLException
     */
    public void insert(String sql,boolean closeConnection) throws SQLException{
        ResultSet exe = executer(sql, 1, closeConnection);
        AlertMessage.LOG(LOG_TITLE, "Inserted successfully!");
    }

    /**
     * Update the table's values
     * @param TABELA Table's name
     * @param SET Values to be changed
     * @param WHERE Coluns where the values will be updated
     * @param closeConnection Boolean value for close the Database connection
     * @throws java.sql.SQLException
     */
    public void update(String TABELA, String SET, String WHERE, boolean closeConnection) throws SQLException {
        String tmp;
        if (WHERE.equals("")) {
            tmp = "UPDATE " + TABELA + " SET " + SET;
        } else {
            tmp = "UPDATE " + TABELA + " SET " + SET + " WHERE " + WHERE;
        }
        System.out.println(tmp);
        
        executer(tmp, 1, closeConnection);
        AlertMessage.LOG(LOG_TITLE, "Updated successfully!");
    }

    /**
     * Delete data from the table
     * @param TABELA Table's name
     * @param WHERE Coluns and values to be deleted, like "ID=1"
     * @param closeConnection Boolean value for close the Database connection
     * @throws java.sql.SQLException
     */
    public void delete(String TABELA, String WHERE, boolean closeConnection) throws SQLException {
        String tmp;
        if (WHERE.equals("")) {
            tmp = "DELETE FROM " + TABELA;
        } else {
            tmp = "DELETE FROM " + TABELA+" WHERE "+WHERE;
        }
        executer(tmp, 1, closeConnection);
        AlertMessage.LOG(LOG_TITLE, "Excluded successfully!");
    }

    /**
     * Makes a query
     * @param SELECT The coluns of the table to be getted
     * @param FROM Table's name
     * @param WHERE Filter for get a little set. like "LABEL='Grama'"
     * @param closeConnection Boolean value for close the Database connection
     * @return Data set of a query
     * @throws java.sql.SQLException
     */
    public ResultSet consult(String SELECT, String FROM, String WHERE, boolean closeConnection) throws SQLException {
        String tmp;
        if (WHERE.equals("")) {
            tmp = "SELECT " + SELECT + " FROM " + FROM;
        } else {
            tmp = "SELECT " + SELECT + " FROM " + FROM + " WHERE " + WHERE;
        }
        executer(tmp, 0, closeConnection);
        AlertMessage.LOG(LOG_TITLE, "Successfully held consultation!");
        return rs;
    }
    
    /**
     * Makes a query
     * @param SELECT The coluns of the table to be getted
     * @param FROM Table's name
     * @param closeConnection Boolean value for close the Database connection
     * @return Data set of a query
     * @throws java.sql.SQLException
     */
    public ResultSet consult(String SELECT, String FROM, boolean closeConnection) throws SQLException {
        return Database.this.consult(SELECT, FROM, "", closeConnection);
    }

    /**
     * Closes the database connection
     * @throws java.sql.SQLException
     */
    public void closeConection() throws SQLException{
        stmt.close();
        con.close();
        AlertMessage.LOG(LOG_TITLE, "Connection Closed!");
    }
    
    /**
     * Makes a database update or makes a query
     * @param sql SQL code
     * @param tipo 1 for an update or others for a query
     * @param closeConnection Boolean value for close the Database connection
     * @return Data set of a query
     */
    private ResultSet executer(String sql, int tipo, boolean closeConnection) throws SQLException {
        con = DriverManager.getConnection(url);
        
        stmt = con.createStatement();
        rs = null;
        
        if (tipo == 1) {
           stmt.executeUpdate(sql);
        } else {
           rs = stmt.executeQuery(sql);
        }
        if(closeConnection){
            closeConection();
        }
        return rs;
    }
}