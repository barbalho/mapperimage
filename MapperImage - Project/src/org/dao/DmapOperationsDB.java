package org.dao;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.model.DictionaryImageMap;
import org.util.AlertMessage;

/**
 * This class is responsible for controlling the data base adapted to DictionaryImageMap
 * @author Felipe Barbalho Rocha, Raul Silveira Silva
 * @version 04.11.2015
 */
public class DmapOperationsDB extends Database{
    static final String LOG_TITLE = "[DATA-BASE-DMAP]";

    /**
     * construct of DmapOperationsDB
     */
    public DmapOperationsDB() {
        super("org.sqlite.JDBC", "jdbc:sqlite:map_labels.db");
    }
    
    /**
     * Insert a new row in the table with the new information DMAP, with the 
     * restriction of not repeating the nameFile and the label at the same time.
     * @param dictionaryMap Dictionary Image map to save.
     * @param fileDmapPath Address informations
     */
    public void insertDMAP(DictionaryImageMap dictionaryMap, File fileDmapPath) {
        try {
            for (String key :  dictionaryMap.getMapLabels().keySet()) {
                String sql = "INSERT INTO LABELS (LABEL,NAME_FILE,ADDRESS)"+ 
                "SELECT '"+key+"','"+fileDmapPath.getName()+"','"+fileDmapPath.getParent()+"'"+
                "WHERE NOT EXISTS(SELECT 1 FROM LABELS WHERE LABEL='"+key+"'and NAME_FILE='"+fileDmapPath.getName()+"'and ADDRESS='"+fileDmapPath.getParent()+"');";  
                super.insert(sql, true); 
            }
        } catch (SQLException ex) {
            AlertMessage.LOG(LOG_TITLE, "Error inserting Dictionary Image Map!");
        }
    }
    
    /**
     * Consult the table of addresses from labels and nameFiels.
     * @param key label from table.
     * @param nameFile nameFile from table.
     * @return requested address.
     */
    public String consultAddress(String key, String nameFile){
        try {
            ResultSet rs = super.consult("*", "LABELS", "LABEL='"+key+"' COLLATE NOCASE and NAME_FILE='"+nameFile+"' ", false);
            String adrs = rs.getString("ADDRESS")+"/"+rs.getString("NAME_FILE");
            closeConection();
            return adrs;
        } catch (SQLException ex) {
            Logger.getLogger(DmapOperationsDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    /**
     * NamesFiles query table and returns a arrayList.
     * @param key Label from table.
     * @return arrayList NamesFiles.
     */
    public ArrayList<String> consultNamesFiles(String key){
        ArrayList<String> namesFiles = new ArrayList<>();
        try {
            ResultSet rs = super.consult("*", "LABELS", "LABEL='"+key+"' COLLATE NOCASE", false);
            while(rs.next()){
                namesFiles.add(rs.getString("NAME_FILE"));
            }
            closeConection();
        } catch (SQLException ex) {
            Logger.getLogger(DmapOperationsDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return namesFiles;
    }
    
    /**
     * Create a labels table
     */
    public void createTable() {
            String sqlCreation = "CREATE TABLE LABELS ("
                    + "ID        INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + "LABEL     TEXT    NOT NULL,"
                    + "NAME_FILE TEXT    NOT NULL,"
                    + "ADDRESS   TEXT    NOT NULL)";
            super.createTable(sqlCreation);
    }
 
}