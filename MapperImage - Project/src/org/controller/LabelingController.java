package org.controller;

import org.model.IndexMatrix;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.stage.Window;
import org.model.ImageMap;
import org.model.DictionaryImageMap;
import org.util.AlertMessage;
import org.util.FileManagerFX;
import org.util.ImageCustomizerUtil;
import org.util.SerializationUtil;

/**
 * This class controls the image annotation module, equivalent to the labeling panel.
 * @author Felipe Barbalho Rocha, Raul Silveira Silva
 * @version 04.11.2015
 */
public class LabelingController {
    
    static final String DIRETORY_SAVES = System.getProperty("user.dir") + "/saves";
    static final String FILE_EXTENSION_SAVES = "*.dmap";
    static final String ITEM_EXISTS_EXCEPTION = "This item already exists";
    static final String ITEM_EXISTS_EXCEPTION_MESSAGE = "Choose another label for this item";
    static final String EMPTY_TEXT_FIELD_EXCEPTION = "Empty text field";
    static final String EMPTY_TEXT_FIELD_EXCEPTION_MESSAGE = "Add some label in the text field.";
    static final String ERROR_SAVING_FILE_EXCEPTION = "It is not possible to save the file";
    static final String ERROR_SAVING_FILE_EXCEPTION_MESSAGE = "There is nothing to be saved";
    static final String ERROR_READ_FILE_EXCEPTION = "Could not read the file";
    static final String ERROR_READ_FILE_FORMAT_EXCEPTION_MESSAGE = "The file can not be read because there is no support for the format or version";
    static final String NO_DESCRIPTION_AVALIABLE_EXCEPTION_MESSAGE = "No description available.";
    static final String NO_FILE_SELECTED_EXCEPTION_MESSAGE = "No file selected.";
    static final String FILE_IS_EMPTY_EXCEPTION_MESSAGE = "The specified file is empty.";
    static final String DELETE_COMFIRM_DIALOG = "Are you sure you want to delete the label?";
    
    private DictionaryImageMap dictionaryMap;
    private ArrayList<IndexMatrix> pontos = new ArrayList();
    private boolean showLabelmapOption;
    private File fileDmapPath;
    
    private static LabelingController instanceLabelingController;
    
    /**
     * The class Singleton constructor
     */
    private LabelingController() {
        this.fileDmapPath = null;
        showLabelmapOption = false;
        dictionaryMap = null;
    }
    
    public static synchronized LabelingController getInstance() { 
        if (instanceLabelingController == null) {
            instanceLabelingController = new LabelingController();
        } 
        return instanceLabelingController; 
    }


    /**
     * Controls the action of inserting the image into the graphical interface ImageView.
     * @param ivLabelingShowImage ImageView component where the image Is set.
     */
    public void handleLabelingShowImage(ImageView ivLabelingShowImage) {
        if (dictionaryMap != null) {
            Image image = ImageCustomizerUtil.toImage(getImage(showLabelmapOption));
            ivLabelingShowImage.setImage(image);
        }
    }

    /**
     * Controls the action click on the image, composed with events arising (Control+Click for example).
     * @param ivLabelingShowImage ImageView component where the image Is set.
     * @param event event received.
     * @param slLabelingBrightnessLevel Slader for Brightness Level
     */
    public void handleLabelingShowImageCliked(ImageView ivLabelingShowImage, MouseEvent event, Slider slLabelingBrightnessLevel) {
        Image image = ivLabelingShowImage.getImage();
        if (image != null) {
            double widthRender = ivLabelingShowImage.getLayoutBounds().getWidth();
            double heightRender = ivLabelingShowImage.getLayoutBounds().getHeight();
            double widthOriginal = image.getWidth();
            double heightOriginal = image.getHeight();
            double newBrightness = slLabelingBrightnessLevel.getValue();
            int x = (int) ((widthOriginal / widthRender) * event.getX());
            int y = (int) ((heightOriginal / heightRender) * event.getY());
            if (event.isControlDown()) {
                ArrayList<Integer> values = ImageCustomizerUtil.listPointsToListValues(pontos, getDictionaryMap().getImageMap().getMatrixRegionMarkedPixels());
                if (values.contains(getDictionaryMap().getImageMap().getMatrixRegionMarkedPixels()[x][y])) {
                    int index = -1;
                    for (IndexMatrix p : pontos) {
                        index++;
                        if (dictionaryMap.getImageMap().getMatrixRegionMarkedPixels()[x][y] == p.getValue()) {
                            break;
                        }
                    }
                    pontos.remove(index);
                } else {
                    pontos.add(new IndexMatrix(x, y, dictionaryMap.getImageMap().getMatrixRegionMarkedPixels()[x][y]));
                }
            } else {
                pontos.clear();
                pontos.add(new IndexMatrix(x, y, dictionaryMap.getImageMap().getMatrixRegionMarkedPixels()[x][y]));
            }
            BufferedImage bfImage = ImageCustomizerUtil.imageBrightnessFocusInRegionsWithPoint(getImage(showLabelmapOption), dictionaryMap.getImageMap().getMatrixRegionMarkedPixels(), pontos, newBrightness);
            ivLabelingShowImage.setImage(ImageCustomizerUtil.toImage(bfImage));
        }
    }

    /**
     * Controls action when a new image is segmented or loaded by menu, resetting values of the labeling panel, and initializing if any.
     * @param lvLabelingItemsMapped labels list.
     * @param newImageMap new imageMap.
     * @param tabLabeling tab of labeling section.
     */
    public void resetLabeling(ListView lvLabelingItemsMapped, ImageMap newImageMap, Tab tabLabeling) {
        if (dictionaryMap != null) {
            dictionaryMap.setImageMap(null);
            dictionaryMap.setMapLabels(null);
            lvLabelingItemsMapped.getItems().clear();
        }
        intitDictionaryMap(newImageMap);
        tabLabeling.setDisable(false);
    }

    /**
     * It controls the action when a new label is added to labes list panel.
     * @param item new label
     * @param tfLabelingNewItem Input text box for new label
     * @param lvLabelingItemsMapped labels list.
     */
    public void handleButtonLabelingAddItem(TextField tfLabelingNewItem, ListView lvLabelingItemsMapped) {
        List<String> itens = lvLabelingItemsMapped.getItems();
        String item = tfLabelingNewItem.getText().toLowerCase();
//        tfLabelingNewItem.setText(item);
        if ((item.trim()).length() > 0) {
            if (!itens.contains(item)) {
                dictionaryMap.setValueInMaplabels(item, ImageCustomizerUtil.listPointsToListValues(pontos, dictionaryMap.getImageMap().getMatrixRegionMarkedPixels()));
                pontos.clear();
                lvLabelingItemsMapped.getItems().add(item);
                tfLabelingNewItem.setText("");
            } else {
                AlertMessage.showWarnning(null, ITEM_EXISTS_EXCEPTION, ITEM_EXISTS_EXCEPTION_MESSAGE);
            }
        } else {
            AlertMessage.showWarnning(null, EMPTY_TEXT_FIELD_EXCEPTION, EMPTY_TEXT_FIELD_EXCEPTION_MESSAGE);
        }
    }

     /**
     * Controls action when there is keyboard input in the list of labels (the removal of an item, for example).
     * @param lvLabelingItemsMapped labels list.
     * @param code Keyboard KeyCode.
     */
    public void handleLabelingListViewKeyboard(ListView lvLabelingItemsMapped, KeyCode code) {
        if (code == KeyCode.DELETE) {
            boolean response = AlertMessage.confirmDialog(null, DELETE_COMFIRM_DIALOG, null);
            if (response) {
                try {
                    String item = (String) lvLabelingItemsMapped.getFocusModel().getFocusedItem();
                    lvLabelingItemsMapped.getItems().remove(item);
                    dictionaryMap.getMapLabels().remove(item);
                } catch (Exception erro) {
                    AlertMessage.showWarnning(null, NO_DESCRIPTION_AVALIABLE_EXCEPTION_MESSAGE, NO_DESCRIPTION_AVALIABLE_EXCEPTION_MESSAGE);
                }
            }
        }
    }

    /**
     * It controls the action when there is an interaction with the mouse on the labels list, displaying the region labels in ImageView.
     * @param item iten clicked.
     * @param newBrightness new Brightness for region.
     * @param ivLabelingShowImage ImageView component where the image Is set.
     */
    public void handleLabelingListViewMouseClicked(String item, double newBrightness, ImageView ivLabelingShowImage) {
        String key = item;
        if (item != null) {
            ArrayList<Integer> list = dictionaryMap.getMapLabels().get(key);
            pontos.clear();
            pontos.addAll(ImageCustomizerUtil.listValuesToListPoints(list, dictionaryMap.getImageMap().getMatrixRegionMarkedPixels()));
            BufferedImage bfImage = ImageCustomizerUtil.imageBrightnessFocusInRegions(getImage(showLabelmapOption), dictionaryMap.getImageMap().getMatrixRegionMarkedPixels(), list, newBrightness);
            Image image = ImageCustomizerUtil.toImage(bfImage);
            if (image != null) {
                ivLabelingShowImage.setImage(image);
            }
        }
    }

    /**
     * Controls the control action of the brightness slider, increasing or decreasing the highlight of the selected regions.
     * @param newBrightness new Brightness for region.
     * @param keyList key for maped list. 
     * @return image with new Brightness in regions.
     */
    public Image sliderChangeBrightness(double newBrightness, String keyList) {
        if (!pontos.isEmpty()) {
            BufferedImage bfImage = ImageCustomizerUtil.imageBrightnessFocusInRegions(getImage(showLabelmapOption), dictionaryMap.getImageMap().getMatrixRegionMarkedPixels(), ImageCustomizerUtil.listPointsToListValues(pontos, dictionaryMap.getImageMap().getMatrixRegionMarkedPixels()), newBrightness);
            return ImageCustomizerUtil.toImage(bfImage);
        } else if (keyList != null) {
            BufferedImage bfImage = ImageCustomizerUtil.imageBrightnessFocusInRegions(getImage(showLabelmapOption), dictionaryMap.getImageMap().getMatrixRegionMarkedPixels(), dictionaryMap.getMapLabels().get(keyList), newBrightness);
            return ImageCustomizerUtil.toImage(bfImage);
        } else {
            return ImageCustomizerUtil.toImage(getImage(showLabelmapOption));
        }
    }

     /**
     * Controls the action of saving the labels dictionary in a file in the proper format.
     * @return case success
     */
    public boolean menuItemSaveImageMap() {
        try {
            if (dictionaryMap != null) {
                File file = saveDmap(null);
                fileDmapPath = file;
                SerializationUtil.serialize(dictionaryMap, fileDmapPath.getAbsolutePath());
                return true;
            } else {
                throw new NullPointerException();
            }
        } catch (NullPointerException ex) {
            AlertMessage.showWarnning(null, ERROR_SAVING_FILE_EXCEPTION, ERROR_SAVING_FILE_EXCEPTION_MESSAGE);
        } catch (IOException ex) {
            AlertMessage.showWarnning(null, ERROR_SAVING_FILE_EXCEPTION, NO_DESCRIPTION_AVALIABLE_EXCEPTION_MESSAGE);
        } catch (Exception erro) {
            AlertMessage.showWarnning(null, ERROR_SAVING_FILE_EXCEPTION, NO_DESCRIPTION_AVALIABLE_EXCEPTION_MESSAGE);
        }
        return false;
    }

    /**
     * Controls the action of loading a file labels of a dictionary in the proper format selected by the user.
     * @param tabLabeling tab of the labeling panel.
     * @param ivLabelingShowImage ImageView component where the image Is set.
     * @param lvLabelingItemsMapped labels list.
     * @return sucess or not
     */
    public boolean handleMenuItemLoadImageMap(Tab tabLabeling, ImageView ivLabelingShowImage, ListView<String> lvLabelingItemsMapped) {
        Image image = loadImageMap();
        if (image != null) {
            tabLabeling.setDisable(false);
            ivLabelingShowImage.setImage(image);
            lvLabelingItemsMapped.getItems().clear();
            lvLabelingItemsMapped.getItems().addAll(dictionaryMap.getMapLabels().keySet());
            return true;
        }
        return false;
    }

    /**
    * Upload an image from a deserialized object.
    * @return mapped image or highlighted image
    */
    public Image loadImageMap() {       
        try {
            File file = openDmap(null);
            DictionaryImageMap dm = (DictionaryImageMap) SerializationUtil.deserialize(file.getAbsolutePath());
            if (dm != null) {
                dictionaryMap = dm;
                this.fileDmapPath = file;
                return ImageCustomizerUtil.toImage(getImage(showLabelmapOption));
            } else {
                throw new NoSuchElementException();
            }
        } catch (NoSuchElementException erro) {
            AlertMessage.showWarnning(null, ERROR_READ_FILE_EXCEPTION, FILE_IS_EMPTY_EXCEPTION_MESSAGE);
        } catch (NullPointerException erro) {
            AlertMessage.showWarnning(null, ERROR_READ_FILE_EXCEPTION, NO_FILE_SELECTED_EXCEPTION_MESSAGE);
//        } catch (IOException erro) {
//            AlertMessage.showWarnning(null, ERROR_READ_FILE_EXCEPTION, ERROR_READ_FILE_FORMAT_EXCEPTION_MESSAGE);
//        } catch (ClassNotFoundException erro) {
//            AlertMessage.showWarnning(null, ERROR_READ_FILE_EXCEPTION, NO_DESCRIPTION_AVALIABLE_EXCEPTION_MESSAGE);
        } catch (Exception erro) {
            AlertMessage.showWarnning(null, ERROR_READ_FILE_EXCEPTION, NO_DESCRIPTION_AVALIABLE_EXCEPTION_MESSAGE);
        }
        return null;
    }

    /**
     * Open dmap File.
     * @param window reference to page resizing.
     * @return File path.
     */
    public File openDmap(Window window) {
        File file = FileManagerFX.openFile("Open DMAP", DIRETORY_SAVES, FileManagerFX.FORMAT_DMAP, window);
        if (file != null) {
            return (file);
        }
        return null;
    }

    /**
     * Save dmap file.
     * @param window reference to page resizing.
     * @return File path.
     */
    public File saveDmap(Window window) {
        File file = FileManagerFX.saveFile("Save DMAP", DIRETORY_SAVES, FileManagerFX.FORMAT_DMAP, window);
        if (file != null) {
            return file;
        }
        return null;
    }
    
    /**
     * Receives the logical value to showLabelmapOption, and updates the ImageView.
     * @param option Boolean value to showLabelmapOption.
     * @param ivLabelingShowImage ImageView to display targeted image or map rotulos.
     */
    public void isShowLabelmapOption(boolean option, ImageView ivLabelingShowImage) {
         showLabelmapOption = option;
         ivLabelingShowImage.setImage(ImageCustomizerUtil.toImage(getImage(showLabelmapOption)));
    }
    
    /**
     * Gets image to display on the ImageView with dependence of showLabelmapOption to display the map of rotulos, if false displays the image defragmented
     * @param showLabelmapOption Boolean value to display or not the rotulos map (true displays the rotulos map)
     * @return image 
     */
    private BufferedImage getImage(boolean showLabelmapOption){
        if(showLabelmapOption){
            return ImageCustomizerUtil.paintMapLabelGrayGradient(
                dictionaryMap.getImageMap().getMatrixRegionMarkedPixels(), dictionaryMap.getImageMap().getTotalRegions()
             );
        }else{
            return dictionaryMap.getImageMap().getRegionMarkedImage();
        }
    }
    
    /**
     * Initialize the labels panel settings.
     * @param slLabelingBrightnessLevel Slider responsible for the brightness control.
     * @param ivLabelingShowImage ImageView to display targeted image or map rotulos.
     * @param lvLabelingItemsMapped ListView with rotulos.
     */
    public void inicializeLabellingPanel(Slider slLabelingBrightnessLevel, ImageView ivLabelingShowImage, ListView<String> lvLabelingItemsMapped) {
        ObservableList<String> data = FXCollections.observableArrayList();
        lvLabelingItemsMapped.setItems(data);
        
        slLabelingBrightnessLevel.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
                    Image image = sliderChangeBrightness((double) new_val, lvLabelingItemsMapped.getFocusModel().getFocusedItem());
                    if(image != null){
                        ivLabelingShowImage.setImage(image);
                    }
            }
        });
    }

    /**
     * get fileDmapPath
     * @return fileDmapPath
     */
    public File getFileDmapPath() {
        return fileDmapPath;
    }
    
    /**
     * set new value for fileDmapPath.
     * @param fileDmapPath new fileDmapPath.
     */
    public void setFileDmapPath(File fileDmapPath) {
        this.fileDmapPath = fileDmapPath;
    }
    
    /**
     * set DictionaryMap
     * @param dictionaryMap new DictionaryMap.
     */
    public void setDictionaryMap(DictionaryImageMap dictionaryMap) {
        this.dictionaryMap = dictionaryMap;
    }
    
    /**
     * get DictionaryMap
     * @return dictionaryMap
     */
    public DictionaryImageMap getDictionaryMap() {
        return dictionaryMap;
    }

    /**
     * Inicialize DictionaryMap.
     * @param imageMap new imageMap.
     */
    public void intitDictionaryMap(ImageMap imageMap) {
        this.dictionaryMap = new DictionaryImageMap(imageMap);
    }

    /**
     * Return Indexs of Matrix
     * @return Indexs of Matrix
     */
    public ArrayList<IndexMatrix> getPontos() {
        return pontos;
    } 

    /**
     * Set new ArrayList points.
     * @param pontos new ArrayList points.
     */
    public void setPontos(ArrayList<IndexMatrix> pontos) {
        this.pontos = pontos;
    } 
    
}