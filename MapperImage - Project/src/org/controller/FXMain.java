package org.controller;

import java.io.FileInputStream;
import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * This class is responsible to show the graphical user interface.
 * @author Felipe Barbalho Rocha, Raul Silveira Silva
 * @version 04.11.2015
 */
public class FXMain extends Application {

    static final String VIEW_FILE_PATH_1 = "view/FXMLDocument.fxml";
    static final String APLICATION_TITLE = "MapperImage";
    static final String FILE_ICON = System.getProperty("user.dir") + "/icon/icon.png";

    /**
     * Starts the graphical user interface.
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) throws IOException {
        Parent root = null;
        try{
            root = FXMLLoader.load(getClass().getResource(VIEW_FILE_PATH_1));
        }catch(Exception erro){
            System.err.println("Não encontrou o view");
            System.exit(0);
        }
        Scene scene = new Scene(root);
        
        primaryStage.setTitle(APLICATION_TITLE);
        primaryStage.resizableProperty().setValue(Boolean.FALSE);
        primaryStage.setScene(scene);
        try{
            primaryStage.getIcons().add(new Image(new FileInputStream(FILE_ICON)));
        }catch(Exception erro){
            System.err.println("Não encontrou o icone");
        }
        primaryStage.show();
    }

    /**
     * Init the program.
     * @param args param start.
     */
    public static void main(String[] args) {
        launch(args);
    }
}
