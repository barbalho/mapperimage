package org.controller;

import org.util.AlertMessage;

/**
 * This class controls the Menu.
 * @author Felipe Barbalho Rocha, Raul Silveira Silva
 * @version 04.11.2015
 */
public class MenuController {
    
    private static MenuController instanceMenuController;
    
    /**
     * The class Singleton constructor
     */
    private MenuController() {
    }
    
    public static synchronized MenuController getInstance() { 
        if (instanceMenuController == null) {
            instanceMenuController = new MenuController();
        } 
        return instanceMenuController; 
    }
    

    /**
     * Closes the program.
     */
    public void handleMenuItemQuit() {
        boolean close = AlertMessage.confirmDialog(null, "Click OK to exit", "Unsaved information will be lost!");
        if (close) {
            System.exit(0);
        }
    }

    /**
     * Shows About Information
     */
    public void handleMenuItemAbout() {
        AlertMessage.showMessage(null, null, "Desenvolvido por Felipe Barbalho Rocha e Raul Silveira Silva");
    }
}
