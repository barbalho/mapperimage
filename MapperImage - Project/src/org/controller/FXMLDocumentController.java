package org.controller;

import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.regex.Pattern;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import org.model.DictionaryImageMap;
import org.util.ImageCustomizerUtil;
import org.util.SerializationUtil;

/**
 * This class is responsible for controlling the graphical user interface, through delegations to the modules segmentation, annotation and search.
 * @author Felipe Barbalho Rocha, Raul Silveira Silva
 * @version 04.11.2015
 */
public class FXMLDocumentController implements Initializable {
   
    
    /*--------------------------Controladores---------------------------*/
    private final MenuController menuController = MenuController.getInstance();
    private final SegmentationController segmentationController = SegmentationController.getInstance();
    private final LabelingController labelingController = LabelingController.getInstance();
    private final SearchController searchController = SearchController.getInstance();
    
    /*--------------------------Componentes do Menu---------------------------*/
    @FXML
    private MenuItem miSaveImageMap;
    @FXML
    private MenuItem miLoadImageMap;
    @FXML
    private MenuItem miOpenImage;
    
    /*-------------------------Componentes de Segmentation--------------------*/
    @FXML
    private Tab tabSegmentation;
    @FXML
    private Button btSegmentationSegmentIt;
    @FXML
    private Button btSegmentationShowLabelMap;
    @FXML
    private ImageView ivSegmentationShowImage;
    @FXML
    private Slider slSegmentationBlurLevel;
    @FXML
    private Slider slSegmentationColorRadius;
    @FXML
    private Slider slSegmentationMinSize;
    @FXML
    private Label lbSegmentationBlurLevel;
    @FXML
    private Label lbSegmentationColorRadius;
    @FXML
    private Label lbSegmentationMinSize;
    @FXML
    private Label lbSegmentationNumberOfRegions;  
    
    /*-------------------------Componentes de Labeling------------------------*/
    @FXML
    private Slider slLabelingBrightnessLevel;
    @FXML
    private Tab tabLabeling;
    @FXML
    private ImageView ivLabelingShowImage;
    @FXML
    private ListView<String> lvLabelingItemsMapped;
    @FXML
    private Button btLabelingAddItem;
    @FXML
    private TextField tfLabelingNewItem;
    @FXML
    private Label lbLabelingCorrdinatesMouse;
    @FXML
    private ToggleButton tbtLabelingShowLabelMap;
    
    /*---------------------------Componentes de Search------------------------*/
    @FXML
    private Tab tabSearch;
    @FXML
    private ListView<String> lvSearchLabels;
    @FXML
    private TextField tfSearchLabels;
    @FXML
    private Button btSearchLabels;
    @FXML
    private ComboBox cbSearchLabels;
    @FXML
    private ImageView ivSearchShowImage;
    
    /*-------------------------------Ações de Menu----------------------------*/
    
    /**
     * Delegates the action to open image segmentationController.
     * @param event received event
     */
    @FXML
    private void handleMenuItemOpenImage( ActionEvent event ){
        segmentationController.handleMenuItemOpenImage( ivSegmentationShowImage );
    }
    
    /**
     * Delegates saving action the Image Map for labeling Controller.
     * @param event received event
     */
    @FXML
    private void handleMenuItemSaveImageMap(ActionEvent event){
        boolean response = labelingController.menuItemSaveImageMap();
        if(response){
            ivSegmentationShowImage.setImage(null);
            searchController.saveDataBaseLabels(labelingController.getDictionaryMap(), labelingController.getFileDmapPath());
        }
    }
    
    /**
     * Delegates Load action the Image Map for labeling Controller.
     * @param event received event
     */
    @FXML
    private void handleMenuItemLoadImageMap(ActionEvent event){
        boolean sucessLoad = labelingController.handleMenuItemLoadImageMap(tabLabeling, ivLabelingShowImage, lvLabelingItemsMapped);
        if(sucessLoad){
            ivSegmentationShowImage.setImage(null);
            searchController.saveDataBaseLabels(labelingController.getDictionaryMap(), labelingController.getFileDmapPath());
        }    
    }
    
    /**
     * Delegates the action to exit the program menuController
     * @param event received event
     */
    @FXML
    private void handleMenuItemQuit(ActionEvent event){
        menuController.handleMenuItemQuit();
    }
    
    /**
     * delegates the action show the about informations to menuController
     * @param event received event
     */
    @FXML
    private void handleMenuItemAbout(ActionEvent event){
        menuController.handleMenuItemAbout();
    }
    
    
    /*------------------------Ações de Segmentação----------------------------*/
    /**
     * Delegate the action to segment the image segmentationController
     * @param event received event
     */
    @FXML
    private void handleButtonSegmentationSegmentIt(ActionEvent event) {
        boolean segmentSucess = segmentationController.handleButtonSegmentationSegmentIt(ivSegmentationShowImage, slSegmentationBlurLevel, slSegmentationColorRadius, slSegmentationMinSize, lbSegmentationNumberOfRegions);
        if (segmentSucess){
             labelingController.resetLabeling(lvLabelingItemsMapped, segmentationController.getImageMap(),tabLabeling);
        }
    }
    
    /**
     * Delegate the action to show label the image to segmentationController
     * @param event received event
     */
    @FXML
    private void handleButtonSegmentationShowLabelMap(ActionEvent event) {
        segmentationController.handleButtonSegmentationShowLabelMap(ivSegmentationShowImage);
    }
     
    
    /*---------------------------Ações de Rotulação---------------------------*/
    /**
     * Delegate the action to show label the image  in Labeling panel to labelingController
     * @param event received event
     */
    @FXML
    private void handleLabelingToggleButtonShowMapLabel(ActionEvent event){
        labelingController.isShowLabelmapOption(tbtLabelingShowLabelMap.isSelected(), ivLabelingShowImage);
    }
    
    /**
     * Delegate the action to show image in Labeling panel to labelingController.
     * @param event received event.
     */
    @FXML
    private void handleLabelingShowImage(Event event) {
        labelingController.handleLabelingShowImage(ivLabelingShowImage);
    }
    
    /**
     * Delegate the action for clicks on the image to labelingController.
     * @param event received event.
     */
    @FXML 
    private void handleLabelingShowImageCliked(MouseEvent event){
        labelingController.handleLabelingShowImageCliked(ivLabelingShowImage, event, slLabelingBrightnessLevel);
    }
    
    /**
     * Delegate the action for mouse motion events (Moved) to labelingController.
     * @param event received event.
     */
    @FXML 
    private void handleLabelingShowImageMouseMoved(MouseEvent event){
        lbLabelingCorrdinatesMouse.setText("("+(int) event.getX()+","+(int) event.getY()+")");
    }
    
    /**
     * Delegate the action for mouse motion events (Entered) to labelingController.
     * @param event received event.
     */
    @FXML 
    private void handleLabelingShowImageMouseEntered(MouseEvent event){
        ivLabelingShowImage.setCursor(Cursor.CROSSHAIR);
    }
    
    /**
     * Delegate the action for mouse motion events (Exited) to labelingController.
     * @param event received event.
     */
    @FXML 
    private void handleLabelingShowImageMouseExited(MouseEvent event){
        ivLabelingShowImage.setCursor(Cursor.DEFAULT);
        lbLabelingCorrdinatesMouse.setText("off");
    }
    
    /**
     * Delegate the action for mouse motion events (Clicked) to labelingController.
     * @param event received event.
     */
    @FXML 
    private void handleLabelingListViewMouseClicked(MouseEvent event){
        String item = lvLabelingItemsMapped.getFocusModel().getFocusedItem();
        Double brightnessLevel = slLabelingBrightnessLevel.getValue();
        labelingController.handleLabelingListViewMouseClicked(item,slLabelingBrightnessLevel.getValue(),ivLabelingShowImage);
    }
    
    /**
     * Delegate the action for keyboard input in ListView to labelingController.
     * @param event received event.
     */
    @FXML 
    private void handleLabelingListViewKeyboard(KeyEvent event){
        labelingController.handleLabelingListViewKeyboard(lvLabelingItemsMapped, event.getCode());
    }
    
    

    /**
     * Delegate the action when the label add button is pressed to labelingController.
     * @param event received event.
     */
    @FXML
    private void handleButtonLabelingAddItem(ActionEvent event) {
        String newItem = tfLabelingNewItem.getText();
        labelingController.handleButtonLabelingAddItem(tfLabelingNewItem, lvLabelingItemsMapped);
    }
    
    /*---------------------------Ações de Pesquisa---------------------------*/
    /**
     * It is called when you type some text in the search.
     * @param event received event.
     */
    @FXML
    private void handleComboBoxSearchKeyboard(KeyEvent event) {
        searchController.handleComboBoxSearchKeyboard(lvSearchLabels, event.getCode(), cbSearchLabels);
    }
    
    /**
     * It is called when you type some text in the search.
     * @param event received event.
     */
    @FXML
    private void handleComboBoxSearchOnClick(MouseEvent event) {
        searchController.handleComboBoxSearchOnClick(lvSearchLabels, event, cbSearchLabels);
    }
    
    /**
     * It is called when you type some text in the search.
     * @param event received event.
     */
    @FXML
    private void handleSearchListViewKeyboard(KeyEvent event){
        searchController.handleSearchListViewKeyboard(lvSearchLabels, event.getCode(),ivSearchShowImage);
    }
    
    /**
     * It is called when event occurs label selection combobox.
     * @param event ActionEvent from mouse or keyboard.
     */
    @FXML
    private void cbSearchLabelsOnAction(ActionEvent event){
        searchController.cbSearchLabelsOnAction(lvSearchLabels, event, cbSearchLabels);
    }

    /**
     * It is called when you type some text in the search.
     * @param event received event.
     */
    @FXML
    private void handleSearchListViewMouseClicked(MouseEvent event){
        String item = lvSearchLabels.getFocusModel().getFocusedItem();
        
        if(item!=null){
            String dmapFilePath = searchController.handleSearchListViewMouseClicked(item);
            if(dmapFilePath!=null){
                DictionaryImageMap dictionaryMap = (DictionaryImageMap) SerializationUtil.deserialize(dmapFilePath);
                item = item.replace("  (", "#").replace(")", "").split(Pattern.quote("#"))[0];
                ArrayList<Integer> list = dictionaryMap.getMapLabels().get(item);
                BufferedImage bfImage = ImageCustomizerUtil.imageBrightnessFocusInRegions(dictionaryMap.getImageMap().getOriginalImage(), dictionaryMap.getImageMap().getMatrixRegionMarkedPixels(),dictionaryMap.getMapLabels().get(item) ,1.5 );
                if(bfImage!=null){
                    ivSearchShowImage.setImage(ImageCustomizerUtil.toImage(bfImage));
                }
            }
        }
        
    }
    
    /**
     * Standard syntax JAVAFX.
     * @param url Uniform Resource Locator.
     * @param rb contain locale-specific object.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        segmentationController.inicializeSegmentationPanel(slSegmentationBlurLevel, slSegmentationColorRadius, slSegmentationMinSize, lbSegmentationBlurLevel, lbSegmentationColorRadius, lbSegmentationMinSize);
        labelingController.inicializeLabellingPanel(slLabelingBrightnessLevel,ivLabelingShowImage,lvLabelingItemsMapped);
        searchController.inicializeSearchPanel(cbSearchLabels, lvSearchLabels);
    }    
}