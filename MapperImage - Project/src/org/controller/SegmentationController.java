package org.controller;

import br.ufrn.imd.lp2.imagesegmentation.ImageSegmentation;
import java.awt.image.BufferedImage;
import java.io.File;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import org.util.ImageCustomizerUtil;
import org.model.ImageMap;
import org.util.AlertMessage;
import org.util.FileManagerFX;

/**
 * This class controls the image segmentation module, equivalent to the segmentation panel.
 * @author Felipe Barbalho Rocha, Raul Silveira Silva
 * @version 04.11.2015
 */
public class SegmentationController {

    private final String DIRETORY_IMAGES = System.getProperty("user.dir") + "/images";
    private final String NO_IMAGE_EXCEPTION = "It was not possible to segment.";
    private final String NO_IMAGE_EXCEPTION_MESAGE = "It needs to load an image in panel.";
    private final String NO_SEGMENTED_IMAGE_EXCEPTION = "It is not possible to display the map rotulos.";
    private final String NO_SEGMENTED_IMAGE_EXCEPTION_MESAGE = "It needs to load and segment the image before.";
    private final String IMAGE_FORMAT_EXCEPTION = "It was not possible to read the image";
    private final String IMAGE_FORMAT_EXCEPTION_MESAGE = "Error reading the specified image format";

    private ImageMap imageMap;
    private File file;
    
    private static SegmentationController instanceSegmentationController;
    
    /**
     * The class Singleton constructor
     */
    private SegmentationController() {
    }
    
    public static synchronized SegmentationController getInstance() { 
        if (instanceSegmentationController == null) {
            instanceSegmentationController = new SegmentationController();
        } 
        return instanceSegmentationController; 
    }

    /**
     * Opens an image and press the segmentation panel ImageView.
     * @param ivSegmentationShowImage ImageView that that will display image.
     */
    public void handleMenuItemOpenImage(ImageView ivSegmentationShowImage) {
        File fileRead = FileManagerFX.openFile("View Pictures", DIRETORY_IMAGES, FileManagerFX.FORMAT_IMAGE, null);

        if (fileRead != null) {
            Image image = new Image(fileRead.toURI().toString());
            if (!image.errorProperty().getValue()) {
                this.file = fileRead;
                ivSegmentationShowImage.setImage(image);
                setImageMap(null);
            } else {
                AlertMessage.showWarnning(null, IMAGE_FORMAT_EXCEPTION, IMAGE_FORMAT_EXCEPTION_MESAGE);
            }
        }
    }

    /**
     * Segments the image according to parameters.
     * @param ivSegmentationShowImage ImageView that that will display image.
     * @param slSegmentationBlurLevel BlurLevel component parameter.
     * @param slSegmentationColorRadius ColorRadius component parameter.
     * @param slSegmentationMinSize MinSize component parameter.
     * @param lbSegmentationNumberOfRegions Number Of Regions component parameter.
     * @return it was defragmented
     */
    public boolean handleButtonSegmentationSegmentIt(ImageView ivSegmentationShowImage, Slider slSegmentationBlurLevel, Slider slSegmentationColorRadius, Slider slSegmentationMinSize, Label lbSegmentationNumberOfRegions) {
        if (ivSegmentationShowImage.getImage() != null) {
            double blurLevel = slSegmentationBlurLevel.getValue();
            int colorRadius = (int) slSegmentationColorRadius.getValue();
            int minSize = (int) slSegmentationMinSize.getValue();
            Image image = segmentIt(blurLevel, colorRadius, minSize);
            ivSegmentationShowImage.setImage(image);
            lbSegmentationNumberOfRegions.setText(String.valueOf(getImageMap().getTotalRegions()));
            return true;
        } else {
            AlertMessage.showWarnning(null, NO_IMAGE_EXCEPTION, NO_IMAGE_EXCEPTION_MESAGE);
            return false;
        }
    }

    /**
     * Show label the image in ImageView.
     * @param ivSegmentationShowImage ImageView for show image.
     */
    public void handleButtonSegmentationShowLabelMap(ImageView ivSegmentationShowImage) {
        if (imageMap != null) {
            BufferedImage bf = ImageCustomizerUtil.paintMapLabelGrayGradient(imageMap.getMatrixRegionMarkedPixels(), imageMap.getTotalRegions());
            Image image = ImageCustomizerUtil.toImage(bf);
            ivSegmentationShowImage.setImage(image);
        } else {
            AlertMessage.showWarnning(null, NO_SEGMENTED_IMAGE_EXCEPTION, NO_SEGMENTED_IMAGE_EXCEPTION_MESAGE);
        }
    }

    /**
     * Segments the image ImageMap.
     * @param blurLevel blurLevel param of segmentation
     * @param colorRadius colorRadius param of segmentation
     * @param minSize  minSize param of segmentation
     * @return segmented image.
     */
    private Image segmentIt(double blurLevel, int colorRadius, int minSize) {
        try{
            imageMap = new ImageMap(ImageSegmentation.performSegmentation(file.getPath(), blurLevel, colorRadius, minSize),blurLevel, colorRadius, minSize);
        }catch(OutOfMemoryError erro){
            AlertMessage.showWarnning(null, "It was not possible to fragment", "The image is too large.");
        }
        WritableImage wi = new WritableImage(imageMap.getWidth(), imageMap.getHeight());
        Image image = ImageCustomizerUtil.toImage(imageMap.getRegionMarkedImage());
        return image;
    }

    /**
     * Initializes the segmentation panel.
     * @param slSegmentationBlurLevel Slider slSegmentationBlurLevel param
     * @param slSegmentationColorRadius Slider slSegmentationColorRadius param
     * @param slSegmentationMinSize Slider slSegmentationMinSize param
     * @param lbSegmentationBlurLevel Label lbSegmentationBlurLevel param
     * @param lbSegmentationColorRadius Label lbSegmentationColorRadius param
     * @param lbSegmentationMinSize Label lbSegmentationMinSize param
     */
    void inicializeSegmentationPanel(Slider slSegmentationBlurLevel, Slider slSegmentationColorRadius, Slider slSegmentationMinSize, Label lbSegmentationBlurLevel, Label lbSegmentationColorRadius, Label lbSegmentationMinSize) {
        slSegmentationBlurLevel.setMin(0.1);
        slSegmentationBlurLevel.setMax(0.99);
        slSegmentationBlurLevel.setBlockIncrement(0.1);
        slSegmentationBlurLevel.valueProperty().addListener(new ChangeListener<Number>() {

            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                lbSegmentationBlurLevel.setText(String.format("%.2f", newValue));
            }
        });
        slSegmentationColorRadius.setMin(2);
        slSegmentationColorRadius.setMax(255);
        slSegmentationColorRadius.setBlockIncrement(4);
        slSegmentationColorRadius.valueProperty().addListener(new ChangeListener<Number>() {

            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                lbSegmentationColorRadius.setText(String.valueOf(newValue.intValue()));
            }
        });
        slSegmentationMinSize.setMin(20);
        slSegmentationMinSize.setMax(2000);
        slSegmentationMinSize.setBlockIncrement(50);
        slSegmentationMinSize.valueProperty().addListener(new ChangeListener<Number>() {

            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                lbSegmentationMinSize.setText(String.valueOf(newValue.intValue()));
            }
        });
        lbSegmentationBlurLevel.setText(String.format("%.2f", slSegmentationBlurLevel.getMin()));
        lbSegmentationColorRadius.setText(String.valueOf((int) slSegmentationColorRadius.getMin()));
        lbSegmentationMinSize.setText(String.valueOf((int) slSegmentationMinSize.getMin()));
    }
    
    /**
     * get ImageMap.
     * @return ImageMap.
     */
    public ImageMap getImageMap() {
        return imageMap;
    }

    /**
     * Set new imageMap. 
     * @param imageMap new imageMap. 
     */
    public void setImageMap(ImageMap imageMap) {
        this.imageMap = imageMap;
    }

    /**
     * get file.
     * @return file.
     */
    public File getFile() {
        return file;
    }

    /**
     * set new file.
     * @param file new file
     */
    public void setFile(File file) {
        this.file = file;
    }
}
