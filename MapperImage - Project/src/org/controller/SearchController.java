package org.controller;

import org.dao.DmapOperationsDB;
import org.dao.Trie;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import org.model.DictionaryImageMap;


/**
 * This class is responsible for controlling search events.
 * @author Felipe Barbalho Rocha, Raul Silveira Silva
 * @version 04.11.2015
 */
public class SearchController {
    static final String DIRETORY_SAVES = System.getProperty("user.dir") + "/saves";
    
    private final DmapOperationsDB dataBase;
    private final Trie trieDataBaseLabels;
    
    private static SearchController instanceSearchController;
    
    /**
     * The class Singleton constructor
     */
    private SearchController() {
        dataBase = new DmapOperationsDB();
        trieDataBaseLabels = new Trie();
    }
    
    public static synchronized SearchController getInstance() { 
        if (instanceSearchController == null) {
            instanceSearchController = new SearchController();
        } 
        return instanceSearchController; 
    }

    /**
     * Initializes the search panel.
     * @param cbSearchLabels combobox label of the tree.
     * @param lvSearchLabels listView labels.
     */
    public void inicializeSearchPanel(ComboBox cbSearchLabels, ListView lvSearchLabels) {
        dataBase.createTable();
        updateListView();
        ObservableList<String> data = FXCollections.observableArrayList(); 
        data.addAll(trieDataBaseLabels.getWordsFromPrefix(""));
        cbSearchLabels.setItems(data);
        cbSearchLabels.getEditor().onMouseReleasedProperty().setValue(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                if (event.getEventType().getName().equals(MouseEvent.MOUSE_RELEASED.getName())){
                    handleComboBoxSearchOnClick(lvSearchLabels, event, cbSearchLabels);
                }
                
            }
        });
    }
    
    /**
     * Performing the action in listView adding labels according to the label of each DMAPs.
     * @param lvSearchLabels listView labels.
     * @param event ActionEvent chage value on ComboBox.
     * @param cbSearchLabels combobox label of the tree.
     */
    public void cbSearchLabelsOnAction(ListView lvSearchLabels, ActionEvent event, ComboBox cbSearchLabels){
        try{
            String item = cbSearchLabels.valueProperty().getValue().toString(); 
            lvSearchLabels.getItems().clear();
            if(item != null && !lvSearchLabels.getItems().contains(item) && !cbSearchLabels.getItems().isEmpty()){
                DmapOperationsDB dmOpetations = new DmapOperationsDB();
                ArrayList<String> namesFiles = dmOpetations.consultNamesFiles(item);
                for (String namesFile : namesFiles) {
                    String label = item+"  ("+namesFile+")";
                    if(!lvSearchLabels.getItems().contains(label)){
                        lvSearchLabels.getItems().add(label);
                    }
                }
            }
        }catch(Exception ex){
            
        }
    }
    
    /**
     * list of auto update when entering text.
     * @param lvSearchLabels List of labels.
     * @param code Keyboard KeyCode.
     * @param cbSearchLabels combobox label of the tree.
     */
    void handleComboBoxSearchKeyboard(ListView lvSearchLabels, KeyCode code, ComboBox cbSearchLabels) {
        String word = cbSearchLabels.getEditor().getText();
        List<String> labels= trieDataBaseLabels.getWordsFromPrefix(word);

        if(KeyCode.BACK_SPACE==code){
            cbSearchLabels.getItems().clear();
            if(labels!=null){
                //cbSearchLabels.getItems().clear();
                cbSearchLabels.getItems().addAll(labels);
                cbSearchLabels.show();
            } 
        }else if(!code.isArrowKey()){
            cbSearchLabels.getItems().clear();
            if(labels!=null){
                //cbSearchLabels.getItems().clear();
                cbSearchLabels.getItems().addAll(labels);
            }
        }
        cbSearchLabels.show();
    }
    
    /**
     * Carries modellist the combo box, according to prefix searched the tree.
     * @param lvSearchLabels  List of labels.
     * @param code mouse event.
     * @param cbSearchLabels combobox label of the tree.
     */
    void handleComboBoxSearchOnClick(ListView lvSearchLabels, MouseEvent code, ComboBox cbSearchLabels){
        String word = cbSearchLabels.getEditor().getText();
        List<String> labels= trieDataBaseLabels.getWordsFromPrefix(word);
        
        if(labels!=null && cbSearchLabels.getEditor().getSelectedText().isEmpty()){
            cbSearchLabels.getItems().clear();
            cbSearchLabels.getItems().addAll(labels);
            cbSearchLabels.getEditor().setText(word);
            cbSearchLabels.getEditor().end();
        }
        cbSearchLabels.show();
       
    }

    /**
     * Delete the selected label.
     * @param lvSearchLabels List of labels.
     * @param code Pressed key.
     */
    void handleSearchListViewKeyboard(ListView<String> lvSearchLabels, KeyCode code, ImageView ivSearchShowImage) {
        if (code == KeyCode.DELETE) {
            try {
                String item = (String) lvSearchLabels.getFocusModel().getFocusedItem();
                lvSearchLabels.getItems().remove(item);
                ivSearchShowImage.setImage(null);
            } catch (Exception erro) {

            }
        }
    }

    /**
     * Gets the labels from the database.
     * @param item Selected item.
     * @return Path of Dmap file.
     */
    public String handleSearchListViewMouseClicked(String item) {
        item = item.replace("  (", "#").replace(")", "");
        String temp[] = item.split(Pattern.quote("#"));
        if(temp.length>1){
            DmapOperationsDB dmOpetations = new DmapOperationsDB();
            String path = dmOpetations.consultAddress(temp[0],temp[1]);
            return path;
        }
        return null;
    }

    /**
     * Add labels on database.
     * @param dictionaryMap label map.
     * @param fileDmapPath Default path for save Dmap files.
     */
    void saveDataBaseLabels(DictionaryImageMap dictionaryMap, File fileDmapPath) {
        DmapOperationsDB dmOpetations = new DmapOperationsDB();
        dmOpetations.insertDMAP(dictionaryMap, fileDmapPath);
        updateListView();
    }
    
    /**
     * Update database add labels on a trie tree.
     */
    void updateListView(){
        try {
            //Consulta os labels
            ResultSet rs = dataBase.consult("*", "LABELS", false);
            //Serve apara ajustar pasta saves no banco
            Map<Integer,String> aux = new HashMap<>();
            while(rs.next()){
                //verifica se o endereço informado no banco está correto
                int id = rs.getInt(1);
                String address = rs.getString("ADDRESS");
                String nameFile = rs.getString("NAME_FILE");
                String label = rs.getString("LABEL");
                if(new File(address+"/"+nameFile).exists()){
                    //adiciona label a arvore
                    trieDataBaseLabels.insertWord(label);
                    
                //se não está no banco, tenta ver se o arquivo está na pasta padrão (caso tenha sido executado de outro pc ou pasta)
                }else if(new File(DIRETORY_SAVES+"/"+nameFile).exists()){
                    System.out.println("ajudste de endereços: "+address+" -> "+DIRETORY_SAVES);
                    aux.put(id, label);
                }
            }
            dataBase.closeConection();
            for (Object key : aux.keySet()) {
                dataBase.update("LABELS", "ADDRESS='"+DIRETORY_SAVES+"'",  "ID='"+key+"'", false);
                trieDataBaseLabels.insertWord(aux.get(key));
            }
            
        } catch (SQLException ex) {
            System.err.println("Error -> updateListView.SearchController -> Update database add labels on a trie tree");
        }
    }
}