﻿Componentes: Felipe Barbalho Rocha, Raul Silveira Silva

MapperImage - Dist
	Contém a versão de distribuição do sistema com o executável MapperImage.jar

MapperImage - Documents
	Contém arquivos importantes do projeto, como o diagrama de classe no Astah 
	e o relatório.
	
MapperImage - Project
	Contém o projeto do NetbBeans com um README dedicado.