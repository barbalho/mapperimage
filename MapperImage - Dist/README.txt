Componentes: Felipe Barbalho Rocha, Raul Silveira Silva

1.	Dependências:
	a.	JDK 8 [1.8.0_25]

2.	Informaçoes:
	a.	O projeto foi desenvolvido através da IDE do NetBeans versão 8.0.2.
	b.	A GUI foi desenvolvida com JavaFX.
	c. 	O projeto foi testado no sistemas Linux, Windows e Mac OS.
	d.	No Windows o Software funcionou sem nenhum problema, porém no Mac OS
		ao alguns eventos como cancelar a abertura de um arquivo podem travar 		o Software.

3.	Executar o projeto:
	a.	Para executar o projeto pelo net beans, basta abrir o arquivo 'MapperImage' na IDE
	b.	Para executar a versão de distribuição, é importante que esteja todos os pacotes no mesmo diretório:
		b.1		libs: 		as bibliotecas de suporte.
		b.2 	saves:		local onde os arquivos .DMAP são armazenados e carregados (banco de imagens segmentadas).
		b.3 	images:		local onde as imagens fontes estão armazenadas.
		b.4		icon:		onde o icone da aplicação está armazenado.
		b.5		e os arquivos "MapperImage.jar" (executável) e "map_labels.db" arquivo com base de dados do sistema.
		
		Após garantido isso é só executar pelo terminal o executavel: java -jar MapperImage.jar

	
